const express = require('express')
// Mongoose is a package that allows creation of Schemas to model our data structures:
// Mongoose is an ODM library to let our Express.js API manipulate a MongoDB database
const mongoose = require('mongoose')

const app = express()

const port = 4000

// MongoDB Connection
// Connect to the database by passing in our connection string
// !Remember! to replace the password and database name with actual values
// Syntax:
// Mongoose.connect("<MongoDB Connection String>", {options to avoid errors in our connection})
// Connecting to MongoDB Atlas

mongoose.set('strictQuery', true)

mongoose.connect(
  'mongodb+srv://admin:admin123@b248.ctuoffq.mongodb.net/s35?retryWrites=true&w=majority',
  {
    // Allows us to avoid any current and future errors while connecting to MongoDB
    useNewURLParser: true,
    useUnifiedTopology: true,
  }
)
// See notifications for connection success or failure
// Connection to database
// Allows us to handle errors when the initial connection is establish
// This works with on and once Mongoose Methods
let dataBase = mongoose.connection
// error handling in connecting
dataBase.on('error', console.error.bind(console, 'connection error'))
// this will be triggered if the connection is successful
dataBase.once('open', () =>
  console.log(`We're connected to MongoDB Atlas 🥳💯`)
)

// Mongoose Schema
// - determine the structure of the documents to be written in the database
// - acts as blueprints to our data

// Syntax:
// const schemaName = new mongoose.Schema({key: value})
// We just used the Schema() constructor of the Mongoose module to create a new Schema object
// The "new" keyword creates a brand new Schema
// required, it is used to specifiy that a field must not be empty
// default, it is used if a field value is not supplied
const taskSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Task name is required'],
  },
  status: {
    type: String,
    default: 'pending',
  },
})

// Models
// - uses Schema and are used to create or instantiate objects that correspond to our Schema.
// - use Schema and they acts as 'middleman' from the server to our database
// Server > Schema(blueprint) > Database > Collection

// First parameter - collection where to store the data
// Second parameter - specify the Schema(blueprint) of the documents that will be stored in our dataBase

const Task = mongoose.model('Task', taskSchema)

// setup for allowing the server to handle data from requests
// allows your application to read JSON data
app.use(express.json())

// allow our app to read data from forms
app.use(express.urlencoded({ extended: true }))

// Creating a new task
// Business Logic
/*
  1) Add a functionality to check if there are duplicate tasks
    - if the task already exists in the dataBase, we will return an error.
    - if the task doesn't exist in the dataBase, we add it in the dataBase
    2) The task data will be coming from our request body
    3) Create a new Task object with only a "name" field/property
    4) The 'status' property does not need to be provided because because our Schema defaults it to "pending" upon creation of an object
*/

app.post('/tasks', (req, res) => {
  Task.findOne({ name: req.body.name }, (err, result) => {
    if (result != null && result.name == req.body.name)
      return res.send(`Duplicate task found! 🥸`)
    else {
      let newTask = new Task({ name: req.body.name })
      newTask.save((saveErr, savedTask) => {
        if (saveErr) return console.error(saveErr)
        else {
          // console.log(`Task is saved! 🫡`)
          console.log(`${savedTask.name} is saved 🫡`)
          return res.status(201).send(`New task created! 🥳`)
        }
      })
    }
  })
})

// Getting all the tasks
// Business Logic
/*
  1) Retrieve all the documents
  2) If an error is encountered, print the error
  3) If there are no erros found, send a seccuess status back to the client/postman and return an array of documents
*/

app.get('/tasks', (request, response) => {
  Task.find({}, (error, result) => {
    error ? console.log(error) : response.status(200).send(result)
  })
})

// Mini Activity #1
// 1) Create three tasks
// 2) View all your tasks via /tasks
// 3) Send a screencap

// > Ang dali naman sobra?

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, 'cannot be empty! 😠'],
  },
  password: {
    type: String,
    required: [true, 'cannot be empty! 😡'],
  },
})

const User = mongoose.model('User', userSchema)

// Get all users (without "password" and "__v")
app.get('/get-all-users', (request, response) => {
  User.find({}, { password: 0, __v: 0 }, (error, result) => {
    error ? console.log(error) : response.status(201).send(result)
  })
})

// Create a user
app.post('/signup', (request, response) => {
  User.findOne({ username: request.body.username }, (error, result) => {
    if (result != null && request.body.username === result.username)
      response.send(`The username "${result.username}" is already taken 🥸`)
    else {
      let newUser = new User({
        username: request.body.username,
        password: request.body.password,
      })
      newUser.save((savingUserError, savingUserResult) => {
        savingUserError
          ? response.status(400).send(savingUserError.message)
          : response
              .status(201)
              .send(`The username "${request.body.username}" is saved 🫡`)
      })
    }
  })
})

app.delete('/delete-a-user', (request, response) => {
  User.deleteOne({ username: request.body.username }, (error, result) => {
    response.send(`${request.body.username} has been successfully deleted 🤐`)
  })
})

app.listen(port, () =>
  console.log(`Server running at port │█║▌║▌║${port}║▌║▌║█│`)
)
